#!/bin/bash
# This is a replacement script for the harvester that downloads harvested metadata from production!
set -e

BASE="${VLO_DATA_DIRECTORY}"
SOURCES_CONFIG="${SOURCES_CONFIG_FILE}"
DATE=$(date +"%Y%m%d_%H%M")

function log {
	echo "[$(date +"%Y-%m-%d %H:%M:%S")] [$1] $2"
}

function log_info {
        log "INFO" "$1"
}

function init {
	if ! [ -d "${BASE}" ]; then
		log "ERROR" "Base directory '${BASE}' does not exist"
		exit 1
	fi
	
	if ! [ -e "${SOURCES_CONFIG}" ]; then
		log "ERROR" "Sources configuration file '${SOURCES_CONFIG_FILE}' does not exist!"
		exit 1
	fi
}

# $1    name, "clarin", "others"
# $2	download url
function download {
	log_info "[$1] download: $2"
	if [ -f "${BASE}/$1" ]; then
        	rm "${BASE}/${1}"
	fi
	curl -o "${BASE}/$1.tar.bz2" "$2" > "${BASE}/download_curl.log" 2>&1
}

# $1	name, "clarin", "others"
function backup {
	log_info "[$1] backup: ${BASE}/$1.bak.${DATE}"
	if [ -d "${BASE}/${1}" ]; then
        	mv "${BASE}/${1}" "${BASE}/${1}.bak.${DATE}"
	fi
}

# $1 	name, "clarin", "other"
function extract {
	log_info "[$1] extract"
	mkdir "${BASE}/${1}"
	cd "${BASE}/${1}"
	nice -n 10 tar -xf "../${1}.tar.bz2"
}

# $1    name, "clarin", "other"
function cleanup {
	log_info "[${1}] cleanup: ${BASE}/${1}.bak.${DATE}"
	DIR="${BASE}/${1}.bak.${DATE}"
	if [ -d "${DIR}" ]; then
        	rm -r "${DIR}"
	fi
}

# $1    name, "clarin", "others"
# $2    download url
function process {
	download "$1" "$2"
	backup "$1"
	extract "$1"
	cleanup "$1"
}

function process_all {
	log "Reading configuration from '${SOURCES_CONFIG}'"
	pids=()
	grep -e '^[^#]' "${SOURCES_CONFIG}" | while true; do
		if ! read NAME; then
			log_info "Completed reading of configuration"
			log_info "Waiting for process(es) to finish: [${pids[*]}]"
			wait "${pids[@]}"
			log_info "All done"
			break
		fi
	
		if [ -z "${NAME}" ]; then
			log "ERROR" "empty name in config"
			exit 1
		fi
	
		if ! read URL; then
			log "WARNING" "Found name without url: '${NAME}'; skipping"
			continue
		fi
	
		if [ -z "${NAME}" ]; then
			log "ERROR" "empty URL in config"
			exit 1
		fi
	
		log_info "Starting processing of '${NAME}' from '${URL}'"
		process "${NAME}" "${URL}" &
		pids+=("$!")
	done
}

init
process_all

#BEGIN remove me (no questions asked)

if [ -f "${BASE}/clarin-map.csv" ] && ! [ -f "${BASE}/clarin/results/map.csv" ]; then
	cp "${BASE}/clarin-map.csv" "${BASE}/clarin/results/map.csv"
fi

#END remove me
