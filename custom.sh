#!/bin/bash
# This is a replacement script for the harvester that downloads harvested metadata from production!
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

set -e

if [ "$1" = "harvest" ]; then
	shift
	echo "[Run harvester (arguments: $*)]"
	(
		cd "${BASE_DIR}/clarin" \
		 && docker-compose -f docker-compose-harvester.yml up --build --force-recreate
	)
fi

if [ "$1" = "viewer-import" ]; then
	shift
	echo "[Viewer import (arguments: $*)]"
	exit 0
fi
